package com.db.microservice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.client.RestTemplate;

@RestController
public class ClientService {
	
	@Autowired
	private DiscoveryClient discoveryClient;

	@GetMapping(path = "discdemo")
	public String doSomething() {
		String response = "";
		RestTemplate template = new RestTemplate();
		List<ServiceInstance> instances = discoveryClient.getInstances("SPRINGAPP2");
		for (ServiceInstance serviceInstance : instances) {
			response = template.getForEntity(serviceInstance.getUri()+ "/test", String.class).getBody();
			break;
		}
		return response;
	}


}
